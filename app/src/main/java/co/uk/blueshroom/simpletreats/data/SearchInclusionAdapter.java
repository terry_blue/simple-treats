package co.uk.blueshroom.simpletreats.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import co.uk.blueshroom.simpletreats.R;
import co.uk.blueshroom.simpletreats.fragments.SearchInclusionListView.SearchInclusion;

public class SearchInclusionAdapter extends ArrayAdapter<SearchInclusion> {

	
	final static String TAG = "SearchInclusionAdapter";
	List<SearchInclusion> data;
	List<String> baseIngs;
	
	
	
	public SearchInclusionAdapter(Context context, int resource, List<SearchInclusion> objects) {
		super(context, resource, objects);
		
		data = objects;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		final View VIEW = (convertView != null) ? convertView : LayoutInflater.from(getContext()).inflate(R.layout.search_inclusion_item, null);
		
		final Spinner ING_SPINNER = (Spinner)VIEW.findViewById(R.id.searchinclusion_spinner_ingredient);
		final int POS = Integer.valueOf(position);
		VIEW.findViewById(R.id.searchinclusion_imagebutton_remove).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				data.remove(POS);
				notifyDataSetChanged();
			}
			
		});		
		
		ING_SPINNER.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, baseIngs));
		
		ING_SPINNER.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				Spinner incSpinner = (Spinner)VIEW.findViewById(R.id.searchinclusion_spinner_include);
				boolean toInclude = (incSpinner.getSelectedItemPosition() == 0); //First item in the resource string-array is "include..."				
					
				
				data.get(POS).modify(toInclude, baseIngs.get(position));
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		if(baseIngs.contains(data.get(POS).getSearchItem())) {
			for(int i = 0; i < baseIngs.size(); i++) {
				if(data.get(POS).getSearchItem().equals(baseIngs.get(i)))
					ING_SPINNER.setSelection(i);
			}			
		} else {
			data.remove(POS);
			notifyDataSetChanged();
		}
		
		return VIEW;
	}
	
	@Override
	public int getCount() {
		return (data != null) ? data.size() : 0;
	}
	
	public void setBaseIngredients(ArrayList<String> types) {
		DatabaseHelper dbH = new DatabaseHelper(getContext());
		baseIngs = dbH.getBaseIngredientsForTypes(types);
		dbH.close();
		notifyDataSetChanged();
	}
	
	public List<SearchInclusion> getData() {
		return data;
	}
	
	public void setSpinnerAdapter(ArrayAdapter<String> newAdapter) {
		
	}
	
	public void addNewSearchInclusion() {
		data.add(new SearchInclusion(true, baseIngs.get(0)));
		
		notifyDataSetChanged();
	}

}
