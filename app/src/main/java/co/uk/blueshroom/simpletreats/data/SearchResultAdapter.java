package co.uk.blueshroom.simpletreats.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import co.uk.blueshroom.simpletreats.R;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper.SearchResult;

public class SearchResultAdapter extends BaseAdapter {

	ArrayList<SearchResult> data = null;
	
	List<String> ing_excludes = null;
	List<String> type_includes = null;
	
	final Context CXT;
	
	
	public SearchResultAdapter(Context context, Cursor cursor, List<String> ingExcludes, List<String> typeIncludes) {
		CXT = context;
		ing_excludes = ingExcludes;
		type_includes = typeIncludes;
		
		
		data = new ArrayList<SearchResult>();
		
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			String[] base_ings = null;
			boolean isVisible = false;
			
			String[] col_names = cursor.getColumnNames();
			
			for(String s : col_names) {
				if(s.equals(DatabaseHelper.COL_REC_BASE_INGREDIENTS))
					base_ings = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_REC_BASE_INGREDIENTS)).split(DatabaseHelper.DELIMITER);
			}
			
			String type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_REC_TYPE));
			for(String s : typeIncludes) {
				if(s.equals(type)) {
					isVisible = true;
					break;
				}					
			}
			
			if(isVisible && base_ings != null) {
				for(String base : base_ings) {
					for(String exclude : ing_excludes) {
						if(base.equals(exclude)) {
							isVisible = false;
							break;
						}
					}
				}
			}
			
			if(isVisible)
				data.add(new SearchResult(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_REC_ID)), cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_REC_TITLE))));
			
			cursor.moveToNext();
		}
		
	}

	
	@Override
	public int getCount() {
		return data.size();
	}


	@Override
	public Object getItem(int position) {
		return data.get(position);
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = (convertView != null) ? convertView : LayoutInflater.from(CXT).inflate(R.layout.listview_item, null);
		
		((TextView)view.findViewById(R.id.listviewitem_textview_title)).setText(data.get(position).TITLE);
		
		return view;
	}
	
	
	
	
}
