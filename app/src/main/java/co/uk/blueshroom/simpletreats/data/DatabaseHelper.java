package co.uk.blueshroom.simpletreats.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;

public class DatabaseHelper extends DataBaseImporter {

	final private static String DB_NAME = "db_simple_treats";
	final private static int DB_VERSION = 1;
	
	final static String TAG = "DatabaseHelper";
	
	final public static String TABLE_NAME_RECIPES = "RecipeTable";
	final public static String TABLE_NAME_RECIPES_FTS = "RecipeTableFTS";
	final public static String TABLE_NAME_SHOPPING_LIST = "ShoppingTable";
	
	final public static String COL_REC_ID = "_id";
	final public static String COL_REC_TYPE = "TreatType";
	final public static String COL_REC_TITLE = "Title";
	final public static String COL_REC_INGREDIENTS = "Ingredients";
	final public static String COL_REC_BASE_INGREDIENTS = "BaseIngredients";
	final public static String COL_REC_RECIPE = "Recipe";
	final public static String COL_REC_SHARE_URL = "ShareURL";
	final public static String COL_REC_FAVOURITE = "Favourite";
	final public static String COL_REC_TAGS = "Tags";
	
	final public static String COL_SHOPPING_ID = "_id";
	final public static String COL_SHOPPING_RECIPE_ID = "Recipe_id";
	final public static String COL_SHOPPING_ITEM = "Recipe_Item";
	final public static String COL_SHOPPING_DATE_ADDED = "Date_Added";
	final public static String COL_SHOPPING_AMOUNT = "Amount";
	
	
	final public static String TYPE_COOKIE = "Cookie";
	final public static String TYPE_SMOOTHIE = "Smoothie";
	final public static String TYPE_COCKTAIL = "Cocktail";
	final public static String TYPE_LITE_COCKTAIL = "Lite-Cocktail";
	final public static String TYPE_CUPCAKE = "Cupcake";
	final public static String TYPE_FAVS = "FAVORITES";
	final public static String TYPE_ALL = "ALL";
	
	final public static String DELIMITER = ";";
	final public static String SEARCH_PREFIX_INCLUDE = "~in~";
	final public static String SEARCH_PREFIX_EXCLUDE = "~ex~";
	
	final public static String[] RECIPE_TYPES = new String[] { TYPE_COOKIE, TYPE_SMOOTHIE, TYPE_COCKTAIL, TYPE_LITE_COCKTAIL, TYPE_CUPCAKE };
	
	
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void createVirtualTables() {
		sqLiteDb.execSQL("CREATE VIRTUAL TABLE "+TABLE_NAME_RECIPES_FTS+" USING fts3("+COL_REC_ID+", "+COL_REC_TYPE+", "+COL_REC_TITLE+", "+COL_REC_INGREDIENTS+", "+COL_REC_BASE_INGREDIENTS+", "+COL_REC_RECIPE+", "+COL_REC_SHARE_URL+", "+COL_REC_FAVOURITE+", "+COL_REC_TAGS+");");
		
		Cursor cursor = sqLiteDb.rawQuery("SELECT * FROM " + TABLE_NAME_RECIPES, null);
		cursor.moveToFirst();
		
		while(!cursor.isAfterLast()) {
			ContentValues values = new ContentValues();
			
			values.put(COL_REC_ID, cursor.getString(cursor.getColumnIndex(COL_REC_ID)));
			values.put(COL_REC_TYPE, cursor.getString(cursor.getColumnIndex(COL_REC_TYPE)));
			values.put(COL_REC_TITLE, cursor.getString(cursor.getColumnIndex(COL_REC_TITLE)));
			values.put(COL_REC_INGREDIENTS, cursor.getString(cursor.getColumnIndex(COL_REC_INGREDIENTS)));
			values.put(COL_REC_BASE_INGREDIENTS, cursor.getString(cursor.getColumnIndex(COL_REC_BASE_INGREDIENTS)));
			values.put(COL_REC_SHARE_URL, cursor.getString(cursor.getColumnIndex(COL_REC_SHARE_URL)));
			values.put(COL_REC_FAVOURITE, cursor.getString(cursor.getColumnIndex(COL_REC_FAVOURITE)));
			values.put(COL_REC_TAGS, cursor.getString(cursor.getColumnIndex(COL_REC_TAGS)));
			values.put(COL_REC_RECIPE, cursor.getString(cursor.getColumnIndex(COL_REC_RECIPE)));
			
			
			sqLiteDb.insert(TABLE_NAME_RECIPES_FTS, null, values);
			cursor.moveToNext();
		}
	}
	
	public String getRandomRecipeIdFor(String TYPE) {
		Cursor c = sqLiteDb.query(TABLE_NAME_RECIPES, new String[] { COL_REC_ID }, COL_REC_TYPE+"='"+TYPE+"'", null, null, null, null);
		int index = 0;
		do {
			index = new Random().nextInt() % c.getCount();
		} while(index < 0);
		c.moveToPosition(index);
		return c.getString(0);
	}
	
	
	final String[] LISTVIEW_COLUMNS = new String[] { COL_REC_ID, COL_REC_TYPE, COL_REC_TITLE, COL_REC_FAVOURITE };
	
	public Cursor getTreatsListViewCursor(String type) {	
		if(type.equals(TYPE_FAVS))
			return getFavouritesListCursor();
		else if(type.equals(TYPE_ALL))
			return getAllListCursor();
		
		return sqLiteDb.query(TABLE_NAME_RECIPES, LISTVIEW_COLUMNS, COL_REC_TYPE+"='"+type+"'", null, null, null, COL_REC_TITLE);		
	}
	public Cursor getShoppingListItemRecipeCursor(String ingredient) {
		String qString = "SELECT " + TABLE_NAME_RECIPES + "." + COL_REC_ID + ", " + TABLE_NAME_RECIPES + "." + COL_REC_TITLE
				+ " FROM " + TABLE_NAME_RECIPES
				+ " INNER JOIN " + TABLE_NAME_SHOPPING_LIST
				+ " ON " + TABLE_NAME_SHOPPING_LIST + "." + COL_SHOPPING_RECIPE_ID + "=" + TABLE_NAME_RECIPES + "." + COL_REC_ID
				+ " WHERE " + COL_SHOPPING_ITEM + "='" + ingredient + "'"
				+ " GROUP BY " + COL_REC_TITLE
				+ " ORDER BY " + COL_REC_TITLE;
		return sqLiteDb.rawQuery(qString, null);
	}
	public Cursor getFavouritesListCursor() {
		return sqLiteDb.query(TABLE_NAME_RECIPES, LISTVIEW_COLUMNS, COL_REC_FAVOURITE+"='"+Boolean.toString(true)+"'", null, null, null, COL_REC_TITLE);
	}
	public Cursor getAllListCursor() {
		return sqLiteDb.query(TABLE_NAME_RECIPES, LISTVIEW_COLUMNS, null, null, null, null, COL_REC_TITLE);
	}
	public Cursor getRecipeTypeCursor() {
		return sqLiteDb.query(TABLE_NAME_RECIPES, new String[] { COL_REC_ID, COL_REC_TYPE }, null, null, COL_REC_TYPE, null, COL_REC_TYPE);
	}
	public ArrayList<String> getBaseIngredientsForTypes(ArrayList<String> types) {
		final ArrayList<String> BASE_ING = new ArrayList<String>();
		String selection = "";
		for(int i = 0; i < types.size(); i++) {
			selection += COL_REC_TYPE+"='"+types.get(i)+"'";
			if(i < types.size() - 1)
				selection += " OR ";
		}
		
		//Log.e(TAG, "selection: " + selection);
		Cursor cursor = sqLiteDb.query(TABLE_NAME_RECIPES, new String[] { COL_REC_BASE_INGREDIENTS }, /*null*/selection, null, COL_REC_BASE_INGREDIENTS, null, COL_REC_TYPE);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			String raw_ing = cursor.getString(cursor.getColumnIndex(COL_REC_BASE_INGREDIENTS));
			//Log.e(TAG, "raw_ing = " + raw_ing);
			String[] ings = raw_ing.split(DELIMITER);
			for(String s : ings) {
				if(!BASE_ING.contains(s))
					BASE_ING.add(s);
			}
			cursor.moveToNext();
		}
		cursor.close();
		return BASE_ING;
	}
	
	public SimpleRecipe getRecipeForId(String id) {
		String[] columns = new String[] { COL_REC_ID, COL_REC_TYPE, COL_REC_TITLE, COL_REC_INGREDIENTS, COL_REC_RECIPE, COL_REC_SHARE_URL, COL_REC_FAVOURITE, COL_REC_TAGS };
		
		Cursor cursor = sqLiteDb.query(TABLE_NAME_RECIPES, columns, COL_REC_ID+"='"+id+"'", null, null, null, null);
		cursor.moveToFirst();
		
		String[] ings = cursor.getString(cursor.getColumnIndex(COL_REC_INGREDIENTS)).split(DELIMITER);
		
		return new SimpleRecipe(cursor.getString(cursor.getColumnIndex(COL_REC_ID)), cursor.getString(cursor.getColumnIndex(COL_REC_TITLE)), cursor.getString(cursor.getColumnIndex(COL_REC_TYPE)), ings, cursor.getString(cursor.getColumnIndex(COL_REC_RECIPE)), cursor.getString(cursor.getColumnIndex(COL_REC_SHARE_URL)), Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(COL_REC_FAVOURITE))));		
	}
	
	
	public Cursor getShoppingListCursor() {		
		String qString = "SELECT " + TABLE_NAME_SHOPPING_LIST + "." + COL_SHOPPING_ID + ", " + COL_SHOPPING_ITEM + ", " + COL_SHOPPING_DATE_ADDED
				+ " FROM " + TABLE_NAME_SHOPPING_LIST
				+ " INNER JOIN " + TABLE_NAME_RECIPES
				+ " ON " + COL_SHOPPING_RECIPE_ID + "=" + TABLE_NAME_RECIPES + "." + COL_REC_ID
				+ " GROUP BY " + COL_SHOPPING_ITEM 
				+ " ORDER BY " + COL_SHOPPING_ITEM;
		
		return sqLiteDb.rawQuery(qString, null);		
	}
	
	public void addToShoppingList(String rawItemString, String recipe_id) {
		String amnt = "";
		String item = "";
		String[] breakdown = rawItemString.split(" - ");
		
		amnt = (breakdown.length > 1) ? breakdown[0] : "";
		item = (breakdown.length > 1) ? breakdown[1].split(",")[0] : breakdown[0].split(",")[0];		
		
		ContentValues values = new ContentValues();
		
		values.put(COL_SHOPPING_RECIPE_ID.substring(COL_SHOPPING_RECIPE_ID.indexOf(".")+1), recipe_id);
		values.put(COL_SHOPPING_AMOUNT.substring(COL_SHOPPING_AMOUNT.indexOf(".")+1), amnt);
		values.put(COL_SHOPPING_DATE_ADDED.substring(COL_SHOPPING_DATE_ADDED.indexOf(".")+1), System.currentTimeMillis());
		values.put(COL_SHOPPING_ITEM.substring(COL_SHOPPING_ITEM.indexOf(".")+1), item);		
		
		sqLiteDb.insert(TABLE_NAME_SHOPPING_LIST, null, values);
	}
	public boolean removeFromShoppingList(String shoppingItem) {
		return (sqLiteDb.delete(TABLE_NAME_SHOPPING_LIST, COL_SHOPPING_ITEM+"='"+shoppingItem+"'", null) > 0);
	}
	
	public ShoppingListAdapter getShoppingListAdapter() {
		return new ShoppingListAdapter(CXT, getShoppingListCursor(), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
	}
	
	
	public void addRecipeToFavs(String recID, boolean isFav) {
		ContentValues values = new ContentValues();
		values.put(COL_REC_FAVOURITE, Boolean.toString(isFav));
		
		sqLiteDb.update(TABLE_NAME_RECIPES, values, COL_REC_ID+"='"+recID+"'", null);
	}
	
	
	
	public Cursor getSearchResultTitleCursor(String query) {		
		return sqLiteDb.query(TABLE_NAME_RECIPES_FTS, LISTVIEW_COLUMNS, COL_REC_TITLE+" MATCH ?", new String[] { appendWildcard(query) }, null, null, COL_REC_TITLE);		
	}
	public Cursor getSearchResultIngredientCursor(List<String> matches) {		
		Cursor cursor = null;
		StringBuilder builder = new StringBuilder();
		
		if(matches.size() > 1) {
			for(int i = 0; i < matches.size(); i++) {
				builder.append("SELECT " + COL_REC_ID + ", " + COL_REC_TITLE + ", " + COL_REC_BASE_INGREDIENTS + ", " + COL_REC_TYPE
						+ " FROM " + TABLE_NAME_RECIPES_FTS
						+ " WHERE " + COL_REC_BASE_INGREDIENTS
						+ " MATCH " + matches.get(i));
				if(i < matches.size() - 1)
					builder.append(" UNION ");
				else builder.append(" ORDER BY " + COL_REC_TITLE);
			}
			cursor = sqLiteDb.rawQuery(builder.toString(), null);
		} else {				
			cursor = sqLiteDb.query(TABLE_NAME_RECIPES_FTS, new String[] { COL_REC_ID, COL_REC_TITLE, COL_REC_BASE_INGREDIENTS, COL_REC_TYPE }, null, null, null, null, COL_REC_TITLE);			
		}
		
		return cursor; 
	}
	
	
	
	
	
	private String appendWildcard(String query) {
		if (TextUtils.isEmpty(query)) return query;

        final StringBuilder builder = new StringBuilder();
        final String[] splits = TextUtils.split(query, " ");

        for (String split : splits)
          builder.append(split).append("*").append(" ");

        return builder.toString().trim();
	}
	
	public static ArrayList<String> getAllRecipeTypes() {
		ArrayList<String> types = new ArrayList<String>();
		types.add(TYPE_COOKIE);
		types.add(TYPE_CUPCAKE);
		types.add(TYPE_COCKTAIL);
		types.add(TYPE_SMOOTHIE);
		types.add(TYPE_LITE_COCKTAIL);
		
		return types;
	}
	
	
	
	
	
	public static class SimpleRecipe {
		final public String REC_ID;
		final public String TITLE;
		final public String[] INGREDIENTS;		
		final public String SHARE_URL;
		final public String RECIPE;
		final public String TYPE;
		private boolean isFav;
		
		public SimpleRecipe(String rec_id, String title, String type, String[] ings, String recipe, String shareURL, boolean fav) {
			REC_ID = rec_id;
			TITLE = title;
			INGREDIENTS = ings;
			RECIPE = recipe;
			SHARE_URL = shareURL;
			TYPE = type;
			isFav = fav;
		}
		
		public boolean getIsFav() {
			return isFav;
		}
		
		public void setFav(boolean fav) {
			isFav = fav;
		}
		
		public String getReadableType() {
			return TYPE.substring(0, 1).toUpperCase(Locale.US) + TYPE.substring(1).toLowerCase(Locale.US);
		}
		
		public String getIngredientsString() {
			String string = "";
			for(String s : INGREDIENTS) 
				string += s + "\n";
						
			return string;
		}
	}
	
	
	
	public static class ShoppingListItem {
		final public String ITEM;
		final public String RECIPE_ID;
		final public long DATE_ADDED;
		final public String AMOUNT;
		
		public ShoppingListItem(String item, String recipe_id, long date_added, String amount) {
			ITEM = item;
			RECIPE_ID = recipe_id;
			DATE_ADDED = date_added;
			AMOUNT = amount;
		}
	}
	
	public static class SearchResult {
		public final String RECIPE_ID;
		public final String TITLE;
		
		public SearchResult(String recID, String title) {
			RECIPE_ID = recID;
			TITLE = title;
		}
	}
}
