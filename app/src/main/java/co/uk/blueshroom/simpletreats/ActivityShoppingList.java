package co.uk.blueshroom.simpletreats;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;
import co.uk.blueshroom.simpletreats.data.ShoppingListAdapter;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyDisplayAdNotifier;

public class ActivityShoppingList extends ActionBarActivity implements OnItemClickListener, OnItemLongClickListener, TapjoyDisplayAdNotifier {

	final String TAG = "ActivityShoppingList";
	
	boolean actionModeActive = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shopping_list);
		setTitle(R.string.shopping_list);		
		
		TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(this, this);
		
		DatabaseHelper dbH = new DatabaseHelper(this);		
		
		ListView lv = (ListView)findViewById(R.id.shoppinglist_listview_shoppingitems); 
		lv.setAdapter(dbH.getShoppingListAdapter());
		lv.setOnItemLongClickListener(this);
		lv.setOnItemClickListener(this);
		lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		
		dbH.close();
	}
	
	
	
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {		
		startSupportActionMode(new ModeCallBack());
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(actionModeActive) {
			((ShoppingListAdapter)(((ListView)findViewById(R.id.shoppinglist_listview_shoppingitems)).getAdapter())).togglePosition(position);
		} else {
			DatabaseHelper dbH = new DatabaseHelper(getApplicationContext());
			
			Cursor adapterCursor = ((ShoppingListAdapter)(((ListView)findViewById(R.id.shoppinglist_listview_shoppingitems)).getAdapter())).getCursor();		
			final Cursor cursor = dbH.getShoppingListItemRecipeCursor(adapterCursor.getString(adapterCursor.getColumnIndex(DatabaseHelper.COL_SHOPPING_ITEM)));
			
			
			AlertDialog.Builder builder = new AlertDialog.Builder(ActivityShoppingList.this);
			builder.setTitle(R.string.choose_recipe);
			builder.setCursor(cursor, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					cursor.moveToPosition(which);
					
					Intent intent = new Intent(getApplicationContext(), ActivityRecipe.class);
					intent.putExtra(ActivityRecipe.BK_RECIPE_ID, cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_REC_ID)));
					startActivity(intent);
				}
			}, DatabaseHelper.COL_REC_TITLE);	
			
			
			AlertDialog dialog = builder.create();
			dialog.show();
			
			dbH.close();
		}
	}


	
	private class ModeCallBack implements ActionMode.Callback {

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch(item.getItemId()) {
			case R.id.m_shoppinglist_remove: {
				ShoppingListAdapter adapter = (ShoppingListAdapter)(((ListView)findViewById(R.id.shoppinglist_listview_shoppingitems)).getAdapter());
				SparseBooleanArray checked = adapter.getSelectedItems();						
				DatabaseHelper dbH = new DatabaseHelper(ActivityShoppingList.this);
				for(int i = 0; i < checked.size(); i++) {
					int key = checked.keyAt(i);
					if(checked.valueAt(i)) {
						Cursor c = adapter.getCursor();
						c.moveToPosition(key);
						String removeItem = c.getString(c.getColumnIndex(DatabaseHelper.COL_SHOPPING_ITEM));
						dbH.removeFromShoppingList(removeItem);
					}
				}
				
				adapter.changeCursor(dbH.getShoppingListCursor());
				dbH.close();
				mode.finish();
				return true;
			}
			}
			return false;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.getMenuInflater().inflate(R.menu.activity_shopping_list, menu);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			actionModeActive = false;
			
			//Refresh the ListView cursor
			DatabaseHelper dbH = new DatabaseHelper(ActivityShoppingList.this);
			ShoppingListAdapter adapter = ((ShoppingListAdapter)(((ListView)findViewById(R.id.shoppinglist_listview_shoppingitems)).getAdapter()));
			adapter.changeCursor(dbH.getShoppingListCursor());
			adapter.clearSelection();		
			((ListView)findViewById(R.id.shoppinglist_listview_shoppingitems)).setAdapter(adapter);			
			dbH.close();
			
			
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			actionModeActive = true;
			return false;
		}
		
	}

	
	/** TAPJOY METHODS **/
	@Override
	public void getDisplayAdResponse(View adView) {
		((LinearLayout)findViewById(R.id.shopping_linearlayout_adview)).removeAllViews();
		((LinearLayout)findViewById(R.id.shopping_linearlayout_adview)).addView(adView);
		
	}

	@Override
	public void getDisplayAdResponseFailed(String reason) {
		Log.e(TAG, "getDisplayAdResponseFailed: " + reason);		
	}
	
}
