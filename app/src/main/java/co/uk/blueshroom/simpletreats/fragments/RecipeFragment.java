package co.uk.blueshroom.simpletreats.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import co.uk.blueshroom.simpletreats.R;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;
import co.uk.blueshroom.simpletreats.data.IngredientsAdapter;



public class RecipeFragment extends Fragment implements OnItemClickListener, OnItemLongClickListener {

	final public static String TAG = "RecipeFragment";
	final public static String ING_TAG = "ingredient";
	final public static String VF_TAG = "viewflipper_tag";
	
	final private static String BK_PAGE_TITLE = "com.kraftykode.simpletreats.fragments.RecipeFragment.page_title";
	final private static String BK_CONTENT = "com.kraftykode.simpletreats.fragments.RecipeFragment.content";
	final private static String BK_REC_ID = "com.kraftykode.simpletreats.fragments.RecipeFragment.recipe_id";
	
	boolean actionModeActive;
	//private MultiChoiceModeListener am_callback;
	
	public static RecipeFragment getNewInstance(String recipeDbId, String page_title, List<String> content) {
		RecipeFragment frag = new RecipeFragment();
		
		Bundle bdl = new Bundle();
		
		bdl.putString(BK_PAGE_TITLE, page_title);		
		bdl.putStringArrayList(BK_CONTENT, new ArrayList<String>(content));
		bdl.putString(BK_REC_ID, recipeDbId);
		
		frag.setArguments(bdl);
		
		
		
		return frag;
	}		
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.recipe_textview, container, false);
		
		//setActionModeCallback();
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		((TextView)getView().findViewById(R.id.recipe_textview_content_type)).setText(getArguments().getString(BK_PAGE_TITLE));
		
		DatabaseHelper dbH = new DatabaseHelper(getActivity());		
		ArrayList<String> content = getArguments().getStringArrayList(BK_CONTENT);
		LinearLayout svLayout = (LinearLayout)getView().findViewById(R.id.recipe_linearlayout_scrollview_container);
		svLayout.removeAllViews();
		
		if(content.size() > 1) {	
			ListView ingLV = new ListView(getActivity());
			ingLV.setTag(ING_TAG);
			ingLV.setOnItemClickListener(this);
			ingLV.setOnItemLongClickListener(this);
			ingLV.setAdapter(new IngredientsAdapter(getActivity(), content.toArray(new String[content.size()])));
			ingLV.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			
			/*
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				ingLV.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
				ingLV.setMultiChoiceModeListener(am_callback);
			}
			*/
			
			svLayout.addView(ingLV);
			
			((TextView)getView().findViewById(R.id.recipe_textview_basetip)).setText(getResources().getString(R.string.ing_add_tip));
			
		} else if(content.size() == 1) {
			ScrollView sv = new ScrollView(getActivity());
			TextView recTV = new TextView(getActivity());
			recTV.setTextAppearance(getActivity(), R.style.body_text);
			recTV.setText(content.get(0));
			
			sv.addView(recTV);
			svLayout.addView(sv);
			
			((TextView)getView().findViewById(R.id.recipe_textview_basetip)).setText(getResources().getString(R.string.rec_tip));
		}	
		
		dbH.close();
	}


	
	/*
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void setActionModeCallback() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)  {
			am_callback = new MultiChoiceModeListener() {
				
				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {					
					
					return false;
				}
				
				@Override
				public void onDestroyActionMode(ActionMode mode) {
					((IngredientsAdapter)((ListView)getView().findViewWithTag(ING_TAG)).getAdapter()).clearSelection();
					
				}
				
				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					mode.getMenuInflater().inflate(R.menu.textview_ingredients, menu);
					return true;
				}
				
				@Override
				public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
					switch(item.getItemId()) {
					case R.id.m_recipe_ingredients_addtoshoppinglist: {
						ArrayList<String> content = getArguments().getStringArrayList(BK_CONTENT);
						DatabaseHelper dbH = new DatabaseHelper(getActivity());
						ListView ingLV = (ListView)getView().findViewWithTag(ING_TAG);
						SparseBooleanArray checked = ingLV.getCheckedItemPositions();
						for(int i = 0; i < checked.size(); i++) {
							if(checked.valueAt(i)) 
								dbH.addToShoppingList(content.get(checked.keyAt(i)), getArguments().getString(BK_REC_ID));							
						}
						
						Toast.makeText(getActivity(), Integer.toString(checked.size()) + " items added", Toast.LENGTH_SHORT).show();
						
						dbH.close();
						mode.finish();
						return true;
					}
					default: return false;
					}
				}

				@Override
				public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

					ListView ingLV = (ListView)getView().findViewWithTag(ING_TAG);
					IngredientsAdapter adapter = (IngredientsAdapter)ingLV.getAdapter();
					adapter.selectPosition(position, checked);
					
				}
			};
		 } else { //Android version less than 11(Honeycomb) 
			 
			 
			 
		 }
	}
	*/

	


	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {		  
		((ActionBarActivity)getActivity()).startSupportActionMode(new ModeCallBack());		
		return false;
	}

	
	
	private class ModeCallBack implements Callback {
		

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch(item.getItemId()) {
			case R.id.m_recipe_ingredients_addtoshoppinglist: {
				ArrayList<String> content = getArguments().getStringArrayList(BK_CONTENT);
				DatabaseHelper dbH = new DatabaseHelper(getActivity());
				ListView ingLV = (ListView)getView().findViewWithTag(ING_TAG);
				SparseBooleanArray checked = ingLV.getCheckedItemPositions();
				for(int i = 0; i < checked.size(); i++) {
					if(checked.valueAt(i)) 
						dbH.addToShoppingList(content.get(checked.keyAt(i)), getArguments().getString(BK_REC_ID));							
				}
				
				Toast.makeText(getActivity(), Integer.toString(checked.size()) + " items added", Toast.LENGTH_SHORT).show();
				
				dbH.close();
				mode.finish();
				return true;
			}
			default: return false;
			}
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.getMenuInflater().inflate(R.menu.textview_ingredients, menu);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			actionModeActive = false;
			((IngredientsAdapter)((ListView)getView().findViewWithTag(ING_TAG)).getAdapter()).clearSelection();
			
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			actionModeActive = true;
			return false;
		}		
		
		
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(actionModeActive) {
			ListView ingLV = (ListView)getView().findViewWithTag(ING_TAG);
			IngredientsAdapter adapter = (IngredientsAdapter)ingLV.getAdapter();
			adapter.togglePosition(position);
		}
		
	}
	
}
