package co.uk.blueshroom.simpletreats.data;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.uk.blueshroom.simpletreats.R;

public class IngredientsAdapter extends ArrayAdapter<String> {

	final String[] CONTENT;
	SparseBooleanArray selectedItems;
	
	public IngredientsAdapter(Context context, String[] objects) {
		super(context, R.id.recipeing_textview_content, objects);
		
		CONTENT = objects;		
		selectedItems = new SparseBooleanArray();
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if(view == null) 
			view = LayoutInflater.from(getContext()).inflate(R.layout.recipe_ingredients_listview, null);
		
		
		
		view.setBackgroundColor(selectedItems.get(position) ? getContext().getResources().getColor(R.color.bg_highlight): Color.TRANSPARENT);
		((TextView)view.findViewById(R.id.recipeing_textview_content)).setText(CONTENT[position]);
		
		return view;
	}
	
	
	@Override
	public int getCount() {
		return CONTENT.length;
	}
	
	
	public void togglePosition(int position) {
		selectPosition(position, !(selectedItems.get(position)));
	}
	public void selectPosition(int position, boolean value) {
		if(value) selectedItems.put(position, value);
		else selectedItems.delete(position);
		
		notifyDataSetChanged();
	}
	
	public void clearSelection() {
		selectedItems = new SparseBooleanArray();
		notifyDataSetChanged();
	}
	
	public SparseBooleanArray getSelectedItems() {
		return selectedItems;
	}

}
