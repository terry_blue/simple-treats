package co.uk.blueshroom.simpletreats;

import java.util.ArrayList;

import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper.SearchResult;
import co.uk.blueshroom.simpletreats.data.SearchResultAdapter;

import com.tapjoy.TapjoyDisplayAdNotifier;

public class ActivitySearchResults extends ActionBarActivity implements OnItemClickListener, TapjoyDisplayAdNotifier {

	final static String TAG = "ActivitySearchResults";
	final public static String BK_SEARCH_FIELD = "co.uk.blueshroom.simpletreats.ActivitySearchResults.search_field";
	final public static String BK_SEARCH_TYPES = "co.uk.blueshroom.simpletreats.ActivitySearchResults.types";
	SearchResultAdapter resultAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_results);
		
		
		
	}	
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		Intent intent = getIntent();
		DatabaseHelper dbH = new DatabaseHelper(this);
		Cursor cursor = null;		
		
		ArrayList<String> matches = new ArrayList<String>();
		ArrayList<String> excludes = new ArrayList<String>();
		ArrayList<String> type_includes = new ArrayList<String>();
		
		if(intent.hasExtra(BK_SEARCH_FIELD)) {
			
			String[] searchItems = intent.getStringExtra(BK_SEARCH_FIELD).split(DatabaseHelper.DELIMITER);			
			
			for(String s : searchItems) {
				if(s.startsWith(DatabaseHelper.SEARCH_PREFIX_INCLUDE)) 
					matches.add(s.replace((CharSequence)DatabaseHelper.SEARCH_PREFIX_INCLUDE, ""));
				else if(s.startsWith(DatabaseHelper.SEARCH_PREFIX_EXCLUDE)) 
					excludes.add(s.replace((CharSequence)DatabaseHelper.SEARCH_PREFIX_EXCLUDE, ""));					
			}
			
			type_includes = intent.getStringArrayListExtra(BK_SEARCH_TYPES);
			cursor = dbH.getSearchResultIngredientCursor(matches);
			
		} else if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
			type_includes = DatabaseHelper.getAllRecipeTypes();
			cursor = dbH.getSearchResultTitleCursor(intent.getStringExtra(SearchManager.QUERY));
		}
			
		resultAdapter = new SearchResultAdapter(this, cursor, excludes, type_includes);

		if(resultAdapter.getCount() > 1) {
			ListView lv = (ListView)findViewById(R.id.searchresults_listview_searchresults);
			
			lv.setAdapter(resultAdapter);
			lv.setOnItemClickListener(this);
		}
		
		
		dbH.close();
	}	


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		SearchResult searchResult = (SearchResult)(((SearchResultAdapter)((ListView)findViewById(R.id.searchresults_listview_searchresults)).getAdapter()).getItem(position));		
		
		Intent intent = new Intent(this, ActivityRecipe.class);
		intent.putExtra(ActivityRecipe.BK_RECIPE_ID, searchResult.RECIPE_ID);
		startActivity(intent);
	}


	@Override
	public void getDisplayAdResponse(View adView) {
		((LinearLayout)findViewById(R.id.searchresults_linearlayout_adview)).addView(adView);
		
	}


	@Override
	public void getDisplayAdResponseFailed(String reason) {
		// TODO Auto-generated method stub
		
	}
}
