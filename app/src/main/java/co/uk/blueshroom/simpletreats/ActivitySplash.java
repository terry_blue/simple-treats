package co.uk.blueshroom.simpletreats;

import java.lang.ref.SoftReference;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

import co.uk.blueshroom.simpletreats.R;
import co.uk.blueshroom.simpletreats.data.Consts;

import com.tapjoy.TapjoyConnect;

public class ActivitySplash extends Activity {

	ThreadHandler handler;
	
	final String TAG = "ActivitySplash";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		
		TapjoyConnect.requestTapjoyConnect(getApplicationContext(), Consts.TAPJOY_APP_ID, Consts.TAPJOY_SECRET_KEY);
		
		handler = new ThreadHandler(this);
		new TimeThread().start();
		
		
	}

	
	private class TimeThread extends Thread {
		@Override
		public void run() {
			try {
				sleep(2000);
				handler.sendEmptyMessage(0);
			} catch(Exception e) { }
		}
	}
	private static class ThreadHandler extends Handler {
		final SoftReference<ActivitySplash> TARGET;
		
		public ThreadHandler(ActivitySplash target) {
			TARGET = new SoftReference<ActivitySplash>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			final ActivitySplash THIS = TARGET.get();
			if(THIS != null) { 
				THIS.startActivity(new Intent(THIS, ActivityHome.class));
				THIS.finish();
			}
			
		}
	}

}
