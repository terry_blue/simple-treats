package co.uk.blueshroom.simpletreats.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;
import co.uk.blueshroom.simpletreats.data.SearchInclusionAdapter;

public class SearchInclusionListView extends ListView {

	public SearchInclusionListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	
	public void updateIngredientSpinnerAdapter(ArrayList<String> types) {
		((SearchInclusionAdapter)getAdapter()).setBaseIngredients(types);
		requestLayout();
	}
	
	
	public void addNewSearchInclusion() {
		((SearchInclusionAdapter)getAdapter()).addNewSearchInclusion();
	}
	
	
	
	public static class SearchInclusion {
		private boolean toInclude;
		private String searchItem;		
		
		
		public SearchInclusion(boolean include, String item) {
			toInclude = include;
			searchItem = item;			
		}
		
		
		public String getQueryBuild() {
			StringBuilder builder = new StringBuilder();
			builder.append((toInclude) ?  DatabaseHelper.SEARCH_PREFIX_INCLUDE : DatabaseHelper.SEARCH_PREFIX_EXCLUDE);
			builder.append("\"");
			builder.append(searchItem);
			builder.append("*\"");
			
			return builder.toString().trim();
		}
		
		
		
		public void modify(boolean include, String item) {
			toInclude = include;
			searchItem = item;
		}
		
		public String getSearchItem() {
			return searchItem;
		}
	}

}
