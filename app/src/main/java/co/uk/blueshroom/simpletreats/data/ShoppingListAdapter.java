package co.uk.blueshroom.simpletreats.data;

import java.util.Date;

import net.sf.jfuzzydate.FuzzyDateFormat;
import net.sf.jfuzzydate.FuzzyDateFormatter;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.CursorAdapter;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.uk.blueshroom.simpletreats.R;

public class ShoppingListAdapter extends CursorAdapter {

	final String TAG = "ShoppingListAdapter";
	
	private SparseBooleanArray selectedItems;
	
	public ShoppingListAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
		
		selectedItems = new SparseBooleanArray();
	}

	@Override
	public void bindView(View view, Context context, Cursor c) {
		String item = c.getString(c.getColumnIndex(DatabaseHelper.COL_SHOPPING_ITEM));
		
		FuzzyDateFormatter fuzzyFormat = FuzzyDateFormat.getInstance();
		String date = fuzzyFormat.formatDistance(new Date(c.getLong(c.getColumnIndex(DatabaseHelper.COL_SHOPPING_DATE_ADDED))));
		
		view.setBackgroundColor(selectedItems.get(c.getPosition()) ? context.getResources().getColor(R.color.bg_highlight): Color.TRANSPARENT);
		
		((TextView)view.findViewById(R.id.shoppinglist_textview_item)).setText(item);
		((TextView)view.findViewById(R.id.shoppinglist_textview_date)).setText(date);
	
	}

	@Override
	public View newView(Context context, Cursor c, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.shoppinglist_item_layout, parent, false);
		
		
		bindView(view, context, c);
		return view;
	}	
	
	
	public void togglePosition(int position) {
		selectPosition(position, !selectedItems.get(position));
	}
	public void selectPosition(int postion, boolean value) {
		if(value) selectedItems.put(postion, value);
		else selectedItems.delete(postion);
		
		notifyDataSetChanged();
	}
	
	public SparseBooleanArray getSelectedItems() {
		return selectedItems;
	}
	public void clearSelection() {
		selectedItems = new SparseBooleanArray();
		notifyDataSetChanged();
	}
}
