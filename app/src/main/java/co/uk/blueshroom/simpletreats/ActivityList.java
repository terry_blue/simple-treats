package co.uk.blueshroom.simpletreats;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import co.uk.blueshroom.simpletreats.R;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyDisplayAdNotifier;

public class ActivityList extends Activity implements OnItemClickListener, TapjoyDisplayAdNotifier {

	public final static String BK_TYPE_TO_DISPLAY = "com.kraftykode.simpletreats.ActivityList.TypeToDisplay";	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);		
		
		TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(this, this);
	}


	@Override
	public void onResume() {
		super.onResume();
		
		DatabaseHelper dbH = new DatabaseHelper(this);				
		
		String type = getIntent().getStringExtra(BK_TYPE_TO_DISPLAY);
		setTitle(type);
		ListView lv = (ListView)findViewById(R.id.list_listview_content);
		lv.setAdapter(new SimpleCursorAdapter(this, R.layout.listview_item, dbH.getTreatsListViewCursor(type), new String[] { DatabaseHelper.COL_REC_TITLE }, new int[] { R.id.listviewitem_textview_title }, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER));	
		lv.setOnItemClickListener(this);
		
		dbH.close();
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Cursor c = (Cursor)((ListView)findViewById(R.id.list_listview_content)).getItemAtPosition(position);
		
		String dbID = c.getString(c.getColumnIndex(DatabaseHelper.COL_REC_ID));
		c.close();
		
		Intent intent = new Intent(ActivityList.this, ActivityRecipe.class);
		intent.putExtra(ActivityRecipe.BK_RECIPE_ID, dbID);
		startActivity(intent);
		
	}
	
	
	public void setTitle(String title) {
		String actual = "";
		if(title.equals(DatabaseHelper.TYPE_COCKTAIL)) actual = "Cocktails";
		else if(title.equals(DatabaseHelper.TYPE_COOKIE)) actual = "Cookies";
		else if(title.equals(DatabaseHelper.TYPE_CUPCAKE)) actual = "Cupcakes";
		else if(title.equals(DatabaseHelper.TYPE_LITE_COCKTAIL)) actual = "Non Alcoholic Cocktails";
		else if(title.equals(DatabaseHelper.TYPE_SMOOTHIE)) actual = "Smoothies";
		else if(title.equals(DatabaseHelper.TYPE_FAVS)) actual = getResources().getString(R.string.view_favs);
		super.setTitle(actual);
	}


	
	/** TAPJOY METHODS **/
	@Override
	public void getDisplayAdResponse(View adView) {
		((LinearLayout)findViewById(R.id.list_linearlayout_adview)).addView(adView);
		
	}


	@Override
	public void getDisplayAdResponseFailed(String arg0) {
		// TODO Auto-generated method stub
		
	}
}
