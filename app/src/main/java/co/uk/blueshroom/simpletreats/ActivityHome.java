package co.uk.blueshroom.simpletreats;

import java.util.Random;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyDisplayAdNotifier;

public class ActivityHome extends ActionBarActivity implements OnClickListener, TapjoyDisplayAdNotifier {

	final static String TAG = "ActivityHome";
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homepage);
		setTitle("");		
		
		TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(ActivityHome.this, this);		
		
		setClickListeners();
	}

	
	
	
	private void setClickListeners() {				
		findViewById(R.id.homepage_button_browse).setOnClickListener(this);
		findViewById(R.id.homepage_button_search).setOnClickListener(this);
		findViewById(R.id.homepage_button_favs).setOnClickListener(this);
		findViewById(R.id.homepage_button_shoppinglist).setOnClickListener(this);		
	}
	
	@Override
	public void onClick(View v) {	
		DatabaseHelper dbH = new DatabaseHelper(ActivityHome.this);
		Intent intent = null;
		
		switch(v.getId()) {
		case R.id.home_button_lucky_dip: {			//TODO add this button to the homepage layout
			intent = new Intent(getApplicationContext(), ActivityRecipe.class);
			
			int recipeIndex = 0;
			do recipeIndex = new Random().nextInt() % DatabaseHelper.RECIPE_TYPES.length;
			while(recipeIndex < 0);
			
			intent.putExtra(ActivityRecipe.BK_RECIPE_ID, dbH.getRandomRecipeIdFor(DatabaseHelper.RECIPE_TYPES[recipeIndex]));			
			break;		
		}
		case R.id.homepage_button_browse: {
			getAlertDialogForType().show();
			break;
		}
		case R.id.homepage_button_shoppinglist: {
			intent = new Intent(getApplicationContext(), ActivityShoppingList.class);
			break;
		}		
		case R.id.homepage_button_favs: {
			intent = new Intent(getApplicationContext(), ActivityList.class);
			intent.putExtra(ActivityList.BK_TYPE_TO_DISPLAY, DatabaseHelper.TYPE_FAVS);
			break;
		}
		case R.id.homepage_button_search: {
			intent = new Intent(getApplicationContext(), ActivitySearch.class);
			break;
		}
		
		}
		
		dbH.close();
		
		if(intent != null)
			startActivity(intent);	
				
	}
	

	
	private AlertDialog getAlertDialogForType() {
		DatabaseHelper dbH = new DatabaseHelper(this);
		final Cursor TYPE_CURSOR = dbH.getRecipeTypeCursor();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCursor(dbH.getRecipeTypeCursor(), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {				
				TYPE_CURSOR.moveToPosition(which);
				Intent intent = new Intent(getApplicationContext(), ActivityList.class);
				intent.putExtra(ActivityList.BK_TYPE_TO_DISPLAY, TYPE_CURSOR.getString(TYPE_CURSOR.getColumnIndex(DatabaseHelper.COL_REC_TYPE)));
				
				startActivity(intent);				
			}

			
			
		}, DatabaseHelper.COL_REC_TYPE);
		
		
		return builder.create();
	}
	
	/** TAPJOY METHODS **/
	@Override
	public void getDisplayAdResponse(View view) {
		((LinearLayout)findViewById(R.id.homepage_linearlayout_adview)).addView(view);
		
	}


	@Override
	public void getDisplayAdResponseFailed(String reason) {
		Log.e(TAG, "ad failed " + reason);
		
	}
	
	
	
	

	
}
