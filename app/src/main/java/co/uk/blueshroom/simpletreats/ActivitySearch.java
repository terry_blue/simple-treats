package co.uk.blueshroom.simpletreats;

import java.util.ArrayList;
import java.util.List;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.CompoundButton.OnCheckedChangeListener;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;
import co.uk.blueshroom.simpletreats.data.SearchInclusionAdapter;
import co.uk.blueshroom.simpletreats.fragments.SearchInclusionListView;
import co.uk.blueshroom.simpletreats.fragments.SearchInclusionListView.SearchInclusion;

import com.tapjoy.TapjoyDisplayAdNotifier;

public class ActivitySearch extends ActionBarActivity implements OnClickListener, OnCheckedChangeListener, TapjoyDisplayAdNotifier {

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		
		((SearchInclusionListView)findViewById(R.id.search_searchinclusionlistview_searchinclusions)).setAdapter(new SearchInclusionAdapter(this, R.layout.search_inclusion_item, new ArrayList<SearchInclusion>()));
		((SearchInclusionListView)findViewById(R.id.search_searchinclusionlistview_searchinclusions)).updateIngredientSpinnerAdapter(getSelectedSearchTypes());
		
		setListeners();
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_search, menu);
	    MenuItem searchItem = menu.findItem(R.id.m_search_searchview);
	    
	    SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
	    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    
	    MenuItemCompat.setOnActionExpandListener(searchItem, new OnActionExpandListener() {
	    	 @Override
	         public boolean onMenuItemActionCollapse(MenuItem item) {
	            findViewById(R.id.search_button_search).setVisibility(View.VISIBLE);
	             return true;  // Return true to collapse action view
	         }

	         @Override
	         public boolean onMenuItemActionExpand(MenuItem item) {
	        	 findViewById(R.id.search_button_search).setVisibility(View.GONE);
	             return true;  // Return true to expand action view
	         }
	    });
	    
		return super.onCreateOptionsMenu(menu);
	}
	
	
	private void setListeners() {
		findViewById(R.id.search_button_search).setOnClickListener(this);
		findViewById(R.id.search_linearlayout_addingredient_filter).setOnClickListener(this);
		
		
		((CheckBox)findViewById(R.id.search_checkbox_cocktails)).setOnCheckedChangeListener(this);
		((CheckBox)findViewById(R.id.search_checkbox_litecocktails)).setOnCheckedChangeListener(this);
		((CheckBox)findViewById(R.id.search_checkbox_cookies)).setOnCheckedChangeListener(this);
		((CheckBox)findViewById(R.id.search_checkbox_cupcakes)).setOnCheckedChangeListener(this);
		((CheckBox)findViewById(R.id.search_checkbox_smoothies)).setOnCheckedChangeListener(this);
	}
	
		
	
	
	
	
	
	
	@Override
	public void onCheckedChanged(CompoundButton button, boolean checked) {		
		((SearchInclusionListView)findViewById(R.id.search_searchinclusionlistview_searchinclusions)).updateIngredientSpinnerAdapter(getSelectedSearchTypes());
	}



	public ArrayList<String> getSelectedSearchTypes() {
		ArrayList<String> types = new ArrayList<String>();
		
		if(((CheckBox)findViewById(R.id.search_checkbox_cocktails)).isChecked()) 
			types.add(DatabaseHelper.TYPE_COCKTAIL);		
		if(((CheckBox)findViewById(R.id.search_checkbox_litecocktails)).isChecked())
			types.add(DatabaseHelper.TYPE_LITE_COCKTAIL);
		if(((CheckBox)findViewById(R.id.search_checkbox_cookies)).isChecked())
			types.add(DatabaseHelper.TYPE_COOKIE);
		if(((CheckBox)findViewById(R.id.search_checkbox_cupcakes)).isChecked())
			types.add(DatabaseHelper.TYPE_CUPCAKE);
		if(((CheckBox)findViewById(R.id.search_checkbox_smoothies)).isChecked())
			types.add(DatabaseHelper.TYPE_SMOOTHIE);
		
		return types;
	}


	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.search_linearlayout_addingredient_filter: {
			((SearchInclusionListView)findViewById(R.id.search_searchinclusionlistview_searchinclusions)).addNewSearchInclusion();
			break;
		}
		case R.id.search_button_search: {
			List<SearchInclusion> data = ((SearchInclusionAdapter)(((SearchInclusionListView)findViewById(R.id.search_searchinclusionlistview_searchinclusions)).getAdapter())).getData();
			
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < data.size(); i++) {
				builder.append(data.get(i).getQueryBuild());
				if(i < data.size() - 1)
					builder.append(DatabaseHelper.DELIMITER);
			}
			
			Intent intent = new Intent(getApplicationContext(), ActivitySearchResults.class);
			intent.putExtra(ActivitySearchResults.BK_SEARCH_FIELD, builder.toString());
			intent.putExtra(ActivitySearchResults.BK_SEARCH_TYPES, getSelectedSearchTypes());
			
			startActivity(intent);
			
			break;
		}
		}
		
	}



	@Override
	public void getDisplayAdResponse(View adView) {
		((LinearLayout)findViewById(R.id.search_linearlayout_adview)).addView(adView);
		
	}



	@Override
	public void getDisplayAdResponseFailed(String reason) {
		// TODO Auto-generated method stub
		
	}
	
}
