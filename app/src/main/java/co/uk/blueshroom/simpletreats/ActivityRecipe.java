package co.uk.blueshroom.simpletreats;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import co.uk.blueshroom.simpletreats.R;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper;
import co.uk.blueshroom.simpletreats.data.DatabaseHelper.SimpleRecipe;
import co.uk.blueshroom.simpletreats.fragments.RecipeFragment;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyDisplayAdNotifier;

public class ActivityRecipe extends ActionBarActivity implements TapjoyDisplayAdNotifier{

	private static final int NUM_PAGES = 2;
	final static String TAG = "ActivityRecipe";
	
	private ViewPager mPager;
	SimpleRecipe recipe;
	Menu actionbarMenu;
	ShareActionProvider shareActionProvider;
	
	final public static String BK_RECIPE_ID = "com.kraftykode.simpletreats.ActivityRecipe.recipeID";
	
	ArrayList<RecipeFragment> pages;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recipe);
		
		TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(this, this);
		
		DatabaseHelper  dbH = new DatabaseHelper(ActivityRecipe.this);
		recipe = dbH.getRecipeForId(getIntent().getStringExtra(BK_RECIPE_ID));		
		dbH.close();		
		
		setTitle(recipe.TITLE);
		
		pages = new ArrayList<RecipeFragment>();		
		
		for(String s : recipe.INGREDIENTS) {
			TextView tv = new TextView(ActivityRecipe.this);
			tv.setText(s);
		}
			
		ArrayList<String> recArrayList = new ArrayList<String>();
		recArrayList.add(recipe.RECIPE);
		
		pages.add(RecipeFragment.getNewInstance(getIntent().getStringExtra(BK_RECIPE_ID), "Ingredients", Arrays.asList(recipe.INGREDIENTS)));
		pages.add(RecipeFragment.getNewInstance(getIntent().getStringExtra(BK_RECIPE_ID), "Recipe", recArrayList));
		
		mPager = (ViewPager)findViewById(R.id.recipe_viewpager);
		mPager.setAdapter(new SlidePagerAdapter(getSupportFragmentManager()));
		
	}
	
	
	 @Override
	 public void onBackPressed() {		 	
		 if (mPager.getCurrentItem() == 0) super.finish();
	     else mPager.setCurrentItem(mPager.getCurrentItem() - 1);
	        
	    }
	
	 @Override
	 public boolean onCreateOptionsMenu(Menu menu) {
		 actionbarMenu = menu;
		 MenuInflater inflater = getMenuInflater();
		 inflater.inflate(R.menu.activity_recipe, menu);	
		 
		 MenuItem shareMenuItem = menu.findItem(R.id.m_recipe_share);
		 shareActionProvider = (ShareActionProvider)MenuItemCompat.getActionProvider(shareMenuItem);
		 shareActionProvider.setShareIntent(getShareIntent());

		 menu.findItem(R.id.m_recipe_addtofav).setTitle((recipe.getIsFav()) ?  R.string.removefromfavs : R.string.addtofavs);
		 menu.findItem(R.id.m_recipe_addtofav).setIcon((recipe.getIsFav()) ? R.drawable.ic_action_favorite : R.drawable.ic_action_not_favorite); 		 
		 		 
		 return super.onCreateOptionsMenu(menu);
	 }
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
		 DatabaseHelper dbH = new DatabaseHelper(getApplicationContext());
		 switch(item.getItemId()) {
		 case R.id.m_recipe_addtofav:  
			 dbH.addRecipeToFavs(recipe.REC_ID, !recipe.getIsFav());
			 recipe.setFav(!recipe.getIsFav());	
			 actionbarMenu.findItem(R.id.m_recipe_addtofav).setTitle((recipe.getIsFav()) ?  R.string.removefromfavs : R.string.addtofavs);
			 actionbarMenu.findItem(R.id.m_recipe_addtofav).setIcon((recipe.getIsFav()) ? R.drawable.ic_action_favorite : R.drawable.ic_action_not_favorite);
			 String toastText = (recipe.getIsFav()) ? getResources().getString(R.string.addedtofavs) : getResources().getString(R.string.removedfromfavs);
			 Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT).show();
			 
			 break;		 
		 case R.id.m_recipe_share: 
			 Log.e(TAG, "share click");
			 
			 break;		 
		 }
		 
		 dbH.close();			 
		 return super.onOptionsItemSelected(item);
	 }
	 
	 
	 
	
	private class SlidePagerAdapter extends FragmentStatePagerAdapter {
		
		public SlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}
		
		@Override
		public Fragment getItem(int position) {
			return pages.get(position);
		}
		
		@Override
		public int getCount() {
			return NUM_PAGES;
		}
	}
	
	
	
	public String getRecipeShareString() {
		String ing = "Ingredients";
		String ingString = recipe.getIngredientsString();
		String rec = "Recipe";
		String recString = recipe.RECIPE;
		String footer = "";
		
		return ing + "\n\n" + ingString + "\n\n\n" + rec + "\n\n" + recString + "\n\n\n" + footer;
	}
	
	public Intent getShareIntent() {
		Intent shareIntent = new Intent();
		shareIntent.setAction(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_TEXT, "Recipe for " + recipe.TITLE + " (" + recipe.getReadableType() + ")\n\n\n" + getRecipeShareString());
		
		return shareIntent;
	}


	@Override
	public void getDisplayAdResponse(View adView) {
		((LinearLayout)findViewById(R.id.recipe_linerarlayout_adview)).removeAllViews();
		((LinearLayout)findViewById(R.id.recipe_linerarlayout_adview)).addView(adView);		
	}


	@Override
	public void getDisplayAdResponseFailed(String reason) {
		Log.e(TAG, "getDisplayAdFailed: " + reason);
		
	}
}
