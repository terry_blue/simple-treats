### What is this? ###

* A list of pleasant treats to make including Cookies, Cocktails, Non-Alcoholic Cocktails, Smoothies and Cupcakes
* Search using ingredients.
* Add your favourites to the favourites list
* Add ingredients you don't have to a shopping list for easy remembering later

### How do I get set up? ###

* Head over to the downloads page and download the latest apk.

### Who do I talk to? ###

* For support contact blueshroomuk@gmail.com